import * as api from "../../node_modules/api";

export function post(req, res) {
    const user = req.body;
    api.post('sessions', user).then(response => {
        if (response.user) req.session.user = response.user;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
    }).catch(error => {
        let e = api.handleError(error.message)
        res.statusCode = e.httpStatus
        res.end(JSON.stringify(e))
    });
}