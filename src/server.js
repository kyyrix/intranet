import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import session from 'express-session';
import sessionFileStore from 'session-file-store';
import * as sapper from '@sapper/server';
import bodyParser from 'body-parser'
import { i18nMiddleware } from './i18n.js';
import "smelte/src/tailwind.css";
require('dotenv').config();

const {PORT, NODE_ENV, NOW} = process.env;
const dev = NODE_ENV === 'development';
const FileStore = sessionFileStore(session);
polka() // You can also use Express
    .use(
        bodyParser.json(),
        session({
            secret: 'ky',
            resave: false,
            saveUninitialized: true,
            cookie: {
                maxAge: 31536000
            },
            store: new FileStore({
                path: NOW ? `/tmp/sessions` : `.sessions`
            })
        }),
        compression({threshold: 0}),
        sirv('static', {dev}),
        i18nMiddleware(),
        sapper.middleware({
            session: req => ({
                user: req.session && req.session.user
            })
        })
    )
    .listen(PORT, err => {
        if (err) console.log('error', err);
    });