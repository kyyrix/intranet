describe('Projects', () => {
    beforeEach(() => {
        cy.visit('/')
        const username = "tatjana.volkov";
        const password = "test";
        cy.get('input[aria-label=Username]').type(username)
        cy.get('input[aria-label=Password]').type(password)

    });
    it("Get Projects", function () {
        cy.get('button[type=submit]').click()
        cy.wait(1000)
        cy.server();
        cy.route({
            method: 'GET',
            url: '/projects/*',
        }).then(() => {
            cy.visit('/projects?user=23&year=2017')
            return getProjects()
        })
        function getProjects(){
            return new Cypress.Promise((resolve, reject) => {
                cy.fixture('../fixtures/projects.json').as('getProjects')
                cy.route('GET', '/projects/**', '@getProjects')
                resolve('@getProjects')
            })
        }
        cy.get('table').should('be.visible')
    })
})