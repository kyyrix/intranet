describe('Login test', () => {
	beforeEach(() => {
		cy.visit('/')

	});

	it('Try login', () => {
		const username = "tatjana.volkov";
		const password = "test";
		cy.get('input[aria-label=Username]').type(username)
		cy.get('input[aria-label=Password]').type(password)
		cy.get('button[type=submit]').click()
	});
});