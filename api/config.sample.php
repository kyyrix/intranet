<?php

/* DATABASE
====================================================================================================*/
define('DATABASE_HOSTNAME', '127.0.0.1');
define('DATABASE_USERNAME', 'root');
define('DATABASE_PASSWORD', '');
define('DATABASE_DATABASE', 'intranet');
define('PRODUCTION', false);
define('SESSION_LIFETIME_IN_HOURS', 24);