<?php namespace App;


class SQL
{

    static function getWhere($criteria, $id_field = null)
    {
        $where = '';
        if (!empty($criteria)) {
            if (is_array($criteria)) {
                if (is_numeric(array_keys($criteria)[0])) {
                    $where = "WHERE " . implode(' AND ', $criteria);
                } else {
                    $where = "WHERE " . implode(' AND ', escape($criteria));
                }
            } else if (is_numeric($criteria)) {
                $where = "WHERE $id_field = $criteria";
            } else {
                $where = "WHERE $criteria";
            }
        }

        return $where;
    }
}
