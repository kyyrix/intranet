<?php namespace App;


class Setting
{
    public static function get($name){

        $name = addslashes($name);
        return get_one("SELECT setting_value FROM settings WHERE setting_name = '$name'");
    }

    public static function set($name, $value){

        $name = addslashes($name);
        $value = addslashes($value);
        insert('settings', ['setting_name' => $name, 'setting_value' => $value]);
    }
}