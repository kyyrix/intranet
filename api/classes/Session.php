<?php namespace App;


class Session
{
    public static function create($user)
    {
        // Create session
        $session = [
            'user_id'    => $user['userId'],
            'created_at' => date('Y-m-d H:i:s'),
            'token'      => md5(date('Y-m-d H:i:s'))
        ];

        // Insert session into database
        insert('sessions', $session);

        // Return session data
        return $session;
    }

    public static function delete($token)
    {
        $token = addslashes($token);
        return q("DELETE FROM sessions WHERE `token` ='$token'");
    }

    public static function deleteExpired()
    {
        q("DELETE FROM sessions WHERE created_at < NOW() - INTERVAL " . SESSION_LIFETIME_IN_HOURS . " HOUR");
    }
}
