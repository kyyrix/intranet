<?php namespace App;


class Asset
{
    public static function listing($users) {

        if($users === '0') {
            $where_users = "";
        } else {
            $where_users = "WHERE EREmployee.ID = '$users[employeeId]'";
        }

        return get_all("SELECT
					cpreportrow.ID AS basicAssetId,
					CONCAT(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) AS employee,
					eremployee.code AS employeeCode,
					crcapitalassets.CODE AS assetCode,
					crcapitalassets.NAME AS assetName,
					crcapitalassets.DESCRIPTION AS assetDesc,
					grproject.NAME AS projectName,
					grproject.CODE AS projectCode,
					CAST(round(cpreportrow.DEPRECIATIONRATE, 2) AS DECIMAL(8,1)) AS depreciationRate,
					cpreportrow.IDGRARTICLE AS articleCode,
					DATE_FORMAT(cpreportrow.CHARGETO,'%d.%m.%Y') AS chargeTo,
					DATE_FORMAT(cpreportrow.DOCDATE,'%d.%m.%Y') AS docDate,
					CAST(round(cpreportrow.QUANTITY,2) AS DECIMAL(8,0)) AS quantity,
                    CAST(round(cpreportrow.BASICCOSTBASE,2) AS DECIMAL(8,2)) AS basicCostBase,
                    CAST(round(cpreportrow.AMOUNT,2) AS DECIMAL(8,2)) AS amount,
                    CAST(round(cpreportrow.AMOUNTBASE,2) AS DECIMAL(8,2)) AS amountBase,
                    CAST(round(cpreportrow.AMOUNTYEARBASE,2) AS DECIMAL(8,2)) AS amountYearBase,
                    CAST(round(cpreportrow.AMOUNTMONTHBASE,2) AS DECIMAL(8,2)) AS amountMonthBase,
					cpreportrow.IDCPREPORT,
					braccount.CODE AS accountCode
				FROM 
					crcapitalassets 
					LEFT JOIN cpreportrow ON cpreportrow.IDCRCAPITALASSETS = crcapitalassets.ID
				    LEFT JOIN eremployee ON cpreportrow.IDEREMPLOYEE = eremployee.ID
					LEFT JOIN grproject ON cpreportrow.IDGRPROJECT = GRProject.ID
					LEFT JOIN braccount ON cpreportrow.IDBRACCOUNT = BRAccount.ID
					$where_users
					GROUP BY crcapitalassets.CODE"
        );
    }
}