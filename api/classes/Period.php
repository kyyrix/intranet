<?php


namespace App;


class Period
{

    public string $first_day_of_month;
    public string $last_day_of_month;
    public int $number_of_days;

    public function __construct($year, $month)
    {
        $this->number_of_days = $d = date('t', strtotime("$year-$month"));
        $this->first_day_of_month = "$year-$month-01";
        $this->last_day_of_month = "$year-$month-$d";
    }
}