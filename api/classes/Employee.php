<?php


namespace App;


class Employee
{

    public static function get()
    {
        return get_all("
            SELECT 
                   Id as employeeId, 
                   CODE as employeeCode, 
                   concat(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) as name 
            FROM eremployee 
            WHERE RECORD_STATE != 'A' 
            ORDER BY eremployee.FIRSTNAME, eremployee.SURNAME");
    }
}