<?php namespace App;

class Timesheet
{
    public static function get($criteria = [])
    {
        $where = SQL::getWhere($criteria, "timesheet_id");
        $rows = get_all("
            SELECT intranet_timesheet.*,
                   intranet_timesheet_row.*,
                   grproject.CODE as projectCode,
                   grproject.NAME as projectName,
                   grproject.U_work_days as workDays,
                   concat(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) as employeeName,
                   eremployee.CODE AS employeeCode,
                   eremployee.ID AS employeeId,
                   grproject.U_default_employee AS defaultEmployee
                FROM intranet_timesheet
                LEFT JOIN intranet_timesheet_row USING (timesheet_id) 
                LEFT JOIN eremployee on intranet_timesheet_row.employee_id = eremployee.ID
                LEFT JOIN grproject on intranet_timesheet.project_id = grproject.ID
                LEFT JOIN grcomment on grproject.ID = grcomment.IDGRPROJECT
                LEFT JOIN eremployee as owner on grcomment.IDEREMPLOYEE = owner.ID  
            $where");
        $result = [];
        $p = new Period($rows[0]['year'], $rows[0]['month']);
        $holidays = self::getHolidays($p);
        foreach ($rows as $row) {
            $result["workDays"] = isset($result["workDays"]) ? $result["workDays"] : json_decode($row['workDays']);
            $result["projectId"] = $row['project_id'];
            $result["holidays"] = $holidays;
            $result["month"] = $row['month'];
            $result["year"] = $row['year'];
            $result["projectName"] = $row['projectName'];
            $result["defaultEmployee"] = $row['defaultEmployee'];

            // Skip if timesheet has no employees
            if (!$row['employeeId']) {
                continue;
            }
            $result['employees'][$row['employeeId']] = [
                'employeeName' => $row['employeeName'],
                'employeeCode' => $row['employeeCode'],
                'employeeId' => $row['employeeId'],
                'salary' => $row['salary']
            ];

            for ($day = 1; $day <= $p->number_of_days; $day++) {
                $result['employees'][$row['employeeId']]['days'][$day] = $row['d' . $day];
            }

        }
        $salaryFunds = Salary::getProjectSalaries($result['projectId'], $rows[0]['month'], $rows[0]['year']);
        if (empty($salaryFunds)) {
            stop(428, ["error" => 'Salary fund is undefined']);
        }
        $result['salaryFund'] = $salaryFunds[0]['salaryFund'];
        if (isset($rows[0]['timesheet_id'])) {
            $result['additionalPayments'] = self::getAdditionalPayments($rows[0]['timesheet_id']);
        }
        return $result;
    }

    public static function updateEmployee($day, $employeeId, $dayValue, $id)
    {
        $timesheet_id = (int)$id;
        $value = $dayValue == true ? 1 : 0;
        $field = 'd' . $day;
        q("UPDATE intranet_timesheet_row inner join eremployee on
                    intranet_timesheet_row.employee_id = eremployee.ID SET $field = $value WHERE timesheet_id = $timesheet_id AND eremployee.ID = $employeeId");
        stop(200);
    }

    public static function addEmployee(int $timesheetId, int $employeeId, $additional)
    {
        $table = $additional ? 'intranet_timesheet_additional_payments' : 'intranet_timesheet_row';
        $id = insert($table, ['timesheet_id' => $timesheetId, 'employee_id' => $employeeId]);

        // Add default employee if missing
        if (!$additional) {
            $timesheet = self::get($timesheetId);
            if (!$timesheet['defaultEmployee']) {
                Project::setDefaultEmployee($timesheet['projectId'], $employeeId);
            }
        }
        stop(200, $additional ? $id : '');
    }

    public static function deleteEmployee(int $timesheetId, int $employeeId, $additionalPaymentsId)
    {
        $table = $additionalPaymentsId !== "null" ? "intranet_timesheet_additional_payments WHERE timesheet_additional_payments_id = $additionalPaymentsId AND" : "intranet_timesheet_row WHERE";
        q("DELETE FROM $table timesheet_id = $timesheetId AND employee_id = $employeeId ");
        stop(204);
    }

    public static function salaryFundLimitReached($id, $returnNumbers = false)
    {
        $timesheet = get_first("
            SELECT
            intranet_project_salary_funds.salary_fund as salaryFund,
            SUM(intranet_timesheet_row.salary) as salary
           FROM intranet_timesheet 
            LEFT JOIN intranet_timesheet_row USING (timesheet_id) 
            LEFT JOIN grproject on intranet_timesheet.project_id = grproject.ID
            LEFT JOIN intranet_project_salary_funds on intranet_project_salary_funds.project_id = grproject.ID
              AND CONCAT(intranet_timesheet.year,'-',IF(intranet_timesheet.month >= 10, intranet_timesheet.month, CONCAT('0',intranet_timesheet.month))) >= start_date
              AND CONCAT(intranet_timesheet.year,'-',IF(intranet_timesheet.month >= 10, intranet_timesheet.month, CONCAT('0',intranet_timesheet.month))) <= end_date
           WHERE timesheet_id = $id");
        if ($returnNumbers) {
            return $timesheet['salary'] > $timesheet['salaryFund'] || $timesheet['salaryFund'] === null ? 2 : ($timesheet['salary'] === $timesheet['salaryFund'] ? 1 : 0);
        } else {
            return $timesheet['salary'] > $timesheet['salaryFund'];
        }
    }

    public static function createTimesheet($employeeId, $data)
    {
        $timesheetId = insert('intranet_timesheet', $data);
        if ($employeeId) {
            $workDays = Project::get($data['project_id'])['workDays'];
            if ($workDays) {
                $p = new Period($data['year'], $data['month']);
                $day = 0;
                $workedDays = [];
                for ($date = strtotime($p->first_day_of_month); $date <= strtotime($p->last_day_of_month); $date += 60 * 60 * 24) {
                    $day++;
                    if (in_array(strftime('%w', $date), $workDays)) {
                        $workedDays['d' . $day] = 1;
                    }
                }
                $value = implode(' , ', $workedDays);
                $columns = implode(' , ', array_keys($workedDays));
                q("INSERT INTO intranet_timesheet_row (timesheet_id, employee_id, $columns) VALUES ($timesheetId, $employeeId, $value) ON DUPLICATE KEY UPDATE timesheet_id = $timesheetId, employee_id = $employeeId");
            }
        }
        stop(200, ['timesheetId' => $timesheetId]);
    }

    public static function getHolidays($p)
    {
        $holidays = get_all("SELECT DATE_FORMAT(holiday_date, '%e') AS dayNumber, holiday_name AS holidayName FROM intranet_holidays WHERE holiday_date BETWEEN '$p->first_day_of_month' AND '$p->last_day_of_month'");
        $result = [];
        foreach ($holidays as $holiday) {
            $result[$holiday['dayNumber']] = $holiday['holidayName'];
        }
        return $result;
    }

    public static function assertUserIsOwnerOrAdmin($timesheet_id, $auth)
    {
        if (empty($auth["isAdmin"])) {
            $project = Project::get(Timesheet::get($timesheet_id)['projectId']);
            if ($project['currentForeman']['foremanId'] != $auth["employeeId"]) {
                stop(403, ["error" => "Forbidden"]);
            }
        }
    }

    public static function assertSalaryFundNotExceeded()
    {
        if (Timesheet::salaryFundLimitReached($_GET['id'])) {
            stop(409, ["error" => 'Salary fund limit exceeded']);
        }
    }

    public static function assertWorkdaysAndSalaryFundHasBeenSet($project)
    {
        if ((isset($project['salaryFunds'][0]) && isset($project['salaryFund'])) || !$project['workDays']) {
            stop(428, ["error" => 'Salary fund or work days is undefined']);
        }
    }

    public static function updateEmployeeSalary($employeeId, $salary, $id)
    {
        update("intranet_timesheet_row", ["salary" => $salary], "employee_id = $employeeId AND timesheet_id = $id");
    }

    public static function getAdditionalPayments($timesheetId)
    {
        return get_all("
            SELECT 
                   timesheet_additional_payments_id as additionalPaymentsId,
                   description, 
                   payment, 
                   employee_id as employeeId,
                   CODE as employeeCode,
                   concat(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) as employeeName
            FROM intranet_timesheet_additional_payments
                LEFT JOIN eremployee on intranet_timesheet_additional_payments.employee_id = eremployee.ID 
            WHERE timesheet_id = $timesheetId");
    }

    public static function updateAdditionalPayments($timesheetId, $data, $employeeId, $additionalPaymentId)
    {
        update('intranet_timesheet_additional_payments',
            (array)$data,
            "timesheet_id = $timesheetId AND employee_id = $employeeId AND timesheet_additional_payments_id = $additionalPaymentId");
    }

    public static function checkIfTimesheetLocked($timesheetId)
    {
        $timesheet = self::get($timesheetId);
        $locked = Salary::getLocked($timesheet['year'], $timesheet['month']);
        if (isset($locked[0])) {
            stop(403, ["error" => 'Timesheet has been locked']);
        };
    }
}