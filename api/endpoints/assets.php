<?php
use App\User;
use App\Asset;

/**
 * Returns a list of fixed assets which correspond to the selected employee
 * @example GET /assets
 * @returns { "assets": {...} }
 */
$app->get('#^assets$#', function () {

    if($_GET['user'] === '0'){
        $users = '0';
    } else {
        $users = User::get(["user_id" => $_GET['user']]);
    }
    $assets = Asset::listing($users);
    stop(200, ["assets" => $assets]);
});