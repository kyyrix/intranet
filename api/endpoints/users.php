<?php

use App\Employee;
use App\User;


/**
 * Returns a list of users
 * @example GET /users
 * @returns { "users": {...} }
 */
$app->get('#^users$#', function () {

    stop(200, ["users" => User::getAll()]);
});
$app->get('#^employees$#', function () {

    stop(200, ["employees" => Employee::get()]);
});