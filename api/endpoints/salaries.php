<?php

use App\Timesheet;
use App\Salary;
use App\Project;

$app->get('#^salaries/projects\/?$#', function () {

    $year = empty($_GET['year']) ? date('Y') : (int)$_GET['year'];
    $user_id = $_GET['user_id'];
    $criteria = [
        "year(U_algus) <= $year",
        "(year(U_lopp) >= $year OR year(U_lopp) = 1899)"];
    if ($user_id !== "0") {
        $criteria[] = "user_id = $user_id";
    }
    stop(200, ['projects' => Salary::projects($criteria, $year), 'locked' => Salary::isLocked($year)]);
});

$app->get('#^salaries/timesheets\/?$#', function () {
    $timesheet = Timesheet::get((int)$_GET['id']);
    Timesheet::assertWorkdaysAndSalaryFundHasBeenSet($timesheet);
    stop(200, $timesheet);
});

$app->get('#^salaries/projects/(?<id>\d+)/edit$#', function ($params) {

    $id = $params['id'];

    stop(200, Project::get($id));
});

$app->get('#^salaries/employees\/?$#', function () {
    $year = empty($_GET['year']) ? date('Y') : (int)$_GET['year'];
    $user_id = $_GET['user_id'];
    stop(200, ['employees' => Salary::employeeSalary($user_id, $year), 'locked' => Salary::isLocked($year)]);
});

$app->get('#^salaries/projects/month\/?$#', function () {
    stop(200, Salary::getProjectLastSalary($_GET['id']));
});

$app->post('#^salaries/projects/(?<id>\d+)/salary\/?$#', function ($params) {
    if (empty($this->user["isAdmin"])) {
        stop(403, ["error" => "Forbidden"]);
    }
    $body = [];
    foreach ($this->requestBody as $paramName => $paramValue) {
        if ($paramName === "salaryFund") {
            $body["salary_fund"] = $paramValue;
        } elseif ($paramName === "startDate") {
            $body["start_date"] = $paramValue;
        } elseif ($paramName === "endDate") {
            $body["end_date"] = $paramValue;
        } else {
            continue;
        }
    }
    $body["project_id"] = $params['id'];
    stop(200, Salary::updateSalary($body));
});

$app->post('#^salaries/projects/(?<id>\d+)/edit\/?$#', function ($params) {

    // Stringify workDays for database
    foreach ($this->requestBody as $paramName => $paramValue) {
        if ($paramName === "workDays") {
            $this->requestBody->U_work_days = json_encode($paramValue);
            unset($this->requestBody->workDays);
        } elseif ($paramName === "employeeId") {
            $this->requestBody->U_default_employee = $paramValue;
            unset($this->requestBody->employeeId);
        } else {
            continue;
        }
    }
    Salary::updateProject($this->requestBody, $params['id']);
    stop(200);
});

$app->post('#^salaries/timesheets/update\/?$#', function () {
    $req = $this->requestBody;
    Timesheet::assertUserIsOwnerOrAdmin($_GET['id'], $this->user);
    Timesheet::checkIfTimesheetLocked($_GET['id']);
    if (isset($req->dayValue)) {
        Timesheet::updateEmployee(
            $req->dayNumber,
            $req->employeeId,
            $req->dayValue,
            $_GET['id']);
    } else {
        Timesheet::assertSalaryFundNotExceeded();
        Timesheet::updateEmployeeSalary($req->employeeId, $req->salary, $_GET['id']);
    }
    stop(200);
});

$app->post('#^salaries/timesheets/(?<timesheetId>\d+)/employees/(?<employeeId>\d+)$#', function ($params) {
    Timesheet::assertUserIsOwnerOrAdmin($params['timesheetId'], $this->user);
    Timesheet::checkIfTimesheetLocked($params['timesheetId']);
    Timesheet::addEmployee($params['timesheetId'], $params['employeeId'], $this->requestBody->additional);
});

$app->post('#^salaries/timesheets/(?<timesheetId>\d+)/additional$#', function ($params) {
    Timesheet::assertUserIsOwnerOrAdmin($params['timesheetId'], $this->user);
    Timesheet::checkIfTimesheetLocked($params['timesheetId']);
    $employeeId = $this->requestBody->employeeId;
    unset($this->requestBody->employeeId);
    $additionalPaymentId = $this->requestBody->additionalPaymentId;
    unset($this->requestBody->additionalPaymentId);
    Timesheet::updateAdditionalPayments($params['timesheetId'], $this->requestBody, $employeeId, $additionalPaymentId);
});

$app->post('#^salaries/projects/timesheet/create$#', function () {
    $req = $this->requestBody;
    $project = Project::get($req->project_id, $req->year, $req->month);

    Timesheet::assertWorkdaysAndSalaryFundHasBeenSet($project);
    Timesheet::createTimesheet($project['employeeId'], (array)$req);
});

$app->post('#^salaries/projects/close$#', function () {
    Salary::lockSalaries($this->requestBody->month, $this->requestBody->year, $this->requestBody->isLocked);
});

$app->delete('#^salaries/timesheets/(?<timesheetId>\d+)/employees/(?<employeeId>\d+)$#', function ($params) {
    Timesheet::assertUserIsOwnerOrAdmin($params['timesheetId'], $this->user);
    Timesheet::deleteEmployee($params['timesheetId'], $params['employeeId'], $_GET['id']);
});

$app->delete('#^salaries/projects/(?<id>\d+)/salary/(?<projectSalaryFundId>\d+)$#', function ($params) {
    Salary::deleteSalary($params['projectSalaryFundId']);
    stop(200);
});