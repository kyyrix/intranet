<?php
/**
 * Database functions
 * Not included in class to shorten typing effort.
 */

connect_db();
function connect_db()
{
    global $db;
    @$db = new mysqli(DATABASE_HOSTNAME, DATABASE_USERNAME, DATABASE_PASSWORD);
    if ($connection_error = mysqli_connect_error()) {
        stop(500, ['error' => 'There was an error trying to connect to database at ' . DATABASE_HOSTNAME . ': ' . $connection_error . '']);
        die();
    }

    try {
        mysqli_select_db($db, DATABASE_DATABASE);
    } catch (Exception $e) {
        PRODUCTION
            ? stop(500, ['error' => 'There was a database related error. Try again later'])
            : stop(500, ['error' => mysqli_error($db), 'trace' => trace(1)]);
    }

    mysqli_query($db, "SET SESSION sql_mode='NO_ENGINE_SUBSTITUTION';") or db_error_out();

    // Switch to utf8
    if (!$db->set_charset("utf8")) {
        trigger_error(sprintf("Error loading character set utf8: %s|", $db->error));
        exit();
    }


}

function q($sql, & $query_pointer = NULL, $debug = FALSE)
{
    global $db;
    if ($debug) {
        print "<pre>$sql</pre>";
    }
    $query_pointer = mysqli_query($db, $sql) or db_error_out(',');
    switch (substr($sql, 0, 6)) {
        case 'UPDA':
            exit("q($sql): Please don't use q() for UPDATEs, use update() instead.");
        default:
            return mysqli_affected_rows($db);
    }
}

function get_one($sql, $debug = FALSE)
{
    global $db;

    if ($debug) { // kui debug on TRUE
        print "<pre>$sql</pre>";
    }
    switch (substr($sql, 0, 6)) {
        case 'SELECT':
            $q = mysqli_query($db, $sql) or db_error_out(',');
            $result = mysqli_fetch_array($q);
            return empty($result) ? NULL : $result[0];
        default:
            exit('get_one("' . $sql . '") failed because get_one expects SELECT statement.');
    }
}

function get_all($sql)
{
    global $db;
    $q = mysqli_query($db, $sql) or db_error_out(',');
    while (($result[] = mysqli_fetch_assoc($q)) || array_pop($result)) {
        ;
    }
    return $result;
}

function get_first($sql)
{
    global $db;
    $q = mysqli_query($db, $sql) or db_error_out(',');
    $first_row = mysqli_fetch_assoc($q);
    return empty($first_row) ? array() : $first_row;
}

function db_error_out($sql = null)
{
    global $db;

    // Get the error message
    $error_message = mysqli_error($db);

    // Shorten syntax error messages
    if (strpos($error_message, 'You have an error in SQL syntax') !== FALSE) {
        $error_message = 'Syntax error in ' . substr($error_message, 135);
    }

    // Remove any output generated before the error
    ob_end_clean();

    // Return 500 error
    PRODUCTION
        ? stop(500, ['error' => 'There was a database related error. Try again later'])
        : stop(500, ['error' => $error_message, 'trace' => trace(2)]);

}

/**
 * @param $table string The name of the table to be inserted into.
 * @param $data array Array of data. For example: array('field1' => 'mystring', 'field2' => 3);
 * @return bool|int Returns the ID of the inserted row or FALSE when fails.
 */
function insert($table, $data)
{
    global $db;
    if ($table and is_array($data) and !empty($data)) {
        $values = implode(',', escape($data));
        $sql = "INSERT INTO `{$table}` SET {$values} ON DUPLICATE KEY UPDATE {$values}";
        $q = mysqli_query($db, $sql) or db_error_out(',');
        $id = mysqli_insert_id($db);
        return ($id > 0) ? $id : FALSE;
    } else {
        return FALSE;
    }
}

function update($table, array $data, $where)
{
    global $db;
    if ($table and is_array($data) and !empty($data)) {
        $values = implode(',', escape($data));

        if (isset($where)) {
            $sql = "UPDATE `{$table}` SET {$values} WHERE {$where}";
        } else {
            $sql = "UPDATE `{$table}` SET {$values}";
        }
        mysqli_query($db, $sql) or db_error_out(',');
        return true;
    } else {
        return FALSE;
    }
}

function escape(array $data)
{
    global $db;
    $values = array();
    if (!empty($data)) {
        foreach ($data as $field => $value) {
            if ($value === NULL) {
                $values[] = "`$field`=NULL";

                // Don't escape $field and $value
            } elseif (is_array($value) && isset($value['no_escape_field']) && isset($value['no_escape_value'])) {
                $values[] = $value['no_escape_field'] . "=" .
                    mysqli_real_escape_string($db, $value['no_escape_value']);

                // Don't escape $field
            } elseif (is_array($value) && isset($value['no_escape_field'])) {
                $values[] = $value['no_escape_field'] . "='" .
                    mysqli_real_escape_string($db, $value["value"]) . "'";

                //Don't escape $value
            } elseif (is_array($value) && isset($value['no_escape'])) {
                $values[] = "`$field`=" . mysqli_real_escape_string($db, $value['no_escape_value']);

            } else {
                $values[] = "`$field`='" . mysqli_real_escape_string($db, $value) . "'";
            }
        }
    }
    return $values;
}