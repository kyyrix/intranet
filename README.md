# Intranet

## Introduction
This project aims to facilitate financial data sharing from a company's accounting software to all foremen 
in the company, so that they can be aware how well their projects are doing and verify that the accountant 
has correctly associated transactions with foremen and projects in the accounting software. 

Although the following guide is targeted to Windows users, it's based on Bash so Linux and Mac users will probably 
find it easy to adjust the instructions to their environment.

The included demo API server (the back end) was chosen to be written in PHP but it can be in any language, 
as long as it responds in the way documented (see the section about the documentation).  

## Project setup

#### Prerequisites    
1. __Xampp__ to get PHP for the included example back-end (API)  
[https://www.apachefriends.org/index.html](https://www.apachefriends.org/index.html) (7.2 or better)  
2. __Composer__ for enabling autoloading for the included example API  
[https://getcomposer.org/download/](https://getcomposer.org/download/) (Composer-Setup.exe)  
3. __Git__ to get Bash in Windows and to download the project   
[https://git-scm.com/download](https://git-scm.com/download)   
4. __Node.js__ as a front-end and documentation server dependency  
[https://www.npmjs.com/get-npm](https://www.npmjs.com/get-npm) (*Recommended for most users* version)  



#### Getting the project and creating the config files   
![](docs/images/project_setup.gif)  
##### Steps
1. Open __Git Bash__ from the Windows start menu.  
2. Change the directory to Xampp's document root folder   
```cd /c/xampp/htdocs ```   
3. Clone the project into that folder, using  
 ```git clone https://bitbucket.org/kyyrix/intranet```  
4. Go into newly created project folder  
 ```cd intranet```  
5. Create front-end project settings file from an example  
```cp .env.sample .env ```  
6. Change API_URL in .env file from path.to/api to  
```nano .env```  
```API_URL = http://localhost/intranet/api```   
7. Create back-end project settings file from an example  
```cp api/config.sample.php api/config.php```  
8. All done! Move to the "Creating and populating the database" phase!  

#### Creating and populating the database  
![](docs/images/database_setup.gif)  
##### Steps
1. Run __Xampp Control Panel__ from the Windows Start menu  
2. Click Mysql's __Start__ button  
3. Click the __Shell__ button on the right  
4. Login to mysql  
    ```
    mysql -u root -p
    ```  
5. Press ENTER if it asks for a password  
6. Create the database  
    ```
    CREATE DATABASE intranet;
    ```  
7. Switch to using that database  
    ```
    use intranet;
    ```  
8. Import structure and sample data into the database   
    ```
    source htdocs\intranet\api\doc\database.sql
    ```  
9. You can now close the Shell and move to "Installing dependencies"  
     
#### Installing dependencies  
![](docs/images/running_project.gif)  
##### Steps
  
1. In __Git Bash__, go into project's folder (if you aren't there yet)  
```cd /c/xampp/htdocs/intranet```  

2. Install all front-end dependencies. This can take up to 2 minutes on a slower computer.   
```npm install```
  
3. Change to the back-end directory  
```cd api```
  
4. Install all back-end dependencies  
```composer install```
> **Composer not found error?** Be sure to restart Git Bash after Composer has been installed.
  
## Running the project
##### Steps
1. In __Xampp Control Panel__, start the back end by starting Apache and MySQL.

2. In __Git Bash__, go into project's folder (if you aren't there yet)  
```cd /c/xampp/htdocs/intranet```  

3. Start the front end  
```npm run dev```
  
4. In your browser, open [localhost:3000](http://localhost:3000)
  
5. Login with  
username: ```tatjana.volkov```  
password: ```test```

6. To stop the front-end server, press <kbd>Ctrl+C</kbd> in the Git Bash window and to stop the back end server, stop Apache and Mysql.
> **Seeing content even after logging out?** Open ```node_modules/smelte/src/components/Tabs/Indicator.svelte``` and change ```transition:slide``` to ```transition:slide|local```

## Documentation  
The back end API endpoints are documented using OpenAPI specification which is located at docs/api.json. This 
spec can be visualised using the bundled Swagger UI tool which allows us to test and validate the API by sending
actual requests to the API and compare the actual response with the documented response. All responses the back end
sends must conform to the examples in the documentation.    

### Accessing the documentation  
![](docs/images/swagger_start.gif)  
##### Steps
1. In __Git Bash__, run the bundled Swagger UI server  
```node swagger.js```  
2. Navigate to [localhost:3001](http://localhost:3001)  
3. Move to the step __Getting the authorization token__ to test the endpoints 

### Getting the authorizing token
To get successful responses from the backend, you must pass a token with each request. To get that token follow the next steps:    
![](docs/images/swagger_authorization.gif)   
##### Steps
1. Navigate to [localhost:3001](http://localhost:3001)  
2. Expand ```POST /sessions```  
3. Click the __Try it out__ button  
4. Press the blue __Execute__ button  
5. Copy __token__ field's *value* (without the quotes) from the *Response body* box   
6. Scroll to the top of the page and click on the __Authorize__ button  
7. Paste the token into the input field and press the __Authorize__ button
8. Try now other requests. They should now respond successfully.  

## Deploying to production
The development version of the project is not suitable for production environment. There is a special command which 
generates a production version of the project which does not include dev dependencies like live reloading and is compiled into super tiny
 and blazingly fast bundle. 
   
![](docs/images/project_build.gif)

##### Steps
1. To produce the production version of the front end, in the project's root directory, type the following command:   
    ```npm run build```
    > A small, self-contained node app will be created into `__sapper__/build` directory which can be copied, along with .env, to the live server and run with  
    >```node index.js```
2. To test the production version of the app in your development environment, you can enter this command  
    ```npm start``` 


## Deploying to windows
#### Extra prerequisites
__WNMP__ to get PHP for the included example back-end (API)  
[https://wnmp.x64architecture.com/downloads/](https://wnmp.x64architecture.com/downloads/)  
##### Steps
1. Follow [this](#getting-the-project-and-creating-the-config-files). Instead of htdocs go to ```C:\Wnmp\html``` 
2. Follow [this](#creating-and-populating-the-database) but open WNMP and start MariaDB instead
3. Add/replace to nginx.conf
    ``` 
        server_name intranet.kyyrix.ee
        location / {
           proxy_set_header   X-Forwarded-For $remote_addr;
           proxy_set_header   Host $http_host;
           proxy_pass         http://localhost:3000;
               }
        location /api {
           fastcgi_split_path_info ^(.+\.php)(/.+)$;
           include fastcgi_params;
           fastcgi_pass 127.0.0.1:9001;
           fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
           fastcgi_index index.php;
           try_files $uri $uri/ =404 /api/index.php?$args ;
               }
    ```
4. Making the build persist 
    
    ```npm install -g pm2```
    
    Create directory 
    
    ```C:\pm2```
    
    Create environment variable 
    
    ```PM2_HOME```
    
    Write in admin cmd to see if it works 
    
    ```pm2 save```
    
    Install 
    
    ```npm install -g pm2-windows-service```
    
    From a command prompt, run 
    
    ```pm2-service-install -n PM2```
    
    Answers should be Yes, Yes, enter, No, Yes, enter
    
    If it gets stuck on first yes do the following:
    ```
    npm install -g npm-check-updates
    cd %USERPROFILE%\AppData\Roaming\npm\node_modules\pm2-windows-service
    ncu inquirer -u
    npm install
    ```
5. To run build write 
    ```npm pm2```